#include <iostream>
#include <fstream>
#include <cassert>
#include <sstream>
#include <string.h>
#include <getopt.h>
#include <stdio.h>
#include "system.h"

using namespace std;

int main(int argc, char *argv[])
{

int selection;
Prefetch *prefetch;
prefetch = new NullPrefetch();
  while (1)
    {
      static struct option long_options[] =
        {
          {"prefetcher",  required_argument, 0, 'p'},
          {0, 0, 0, 0}
        };
      int option_index = 0;

      selection = getopt_long (argc, argv, "p", long_options, &option_index);

      if (selection == -1)
      {
        break;
      }

      switch (selection)
      {
        case 'p':
            if (strcmp("NullPrefetch", optarg) == 0)
            {
              break;
            }

            else if (strcmp("AdjPrefetch", optarg) == 0)
            {
              delete prefetch;
              prefetch = new AdjPrefetch();
            }

            else if (strcmp("SeqPrefetch", optarg) == 0)
            {
              delete prefetch;
//          **CONFIGURE N FOR SEQPREFETCH HERE:**
              prefetch = new SeqPrefetch(5);
            }

            else if (strcmp("BestEffortPrefetch", optarg) == 0)
            {
              delete prefetch;
              prefetch = new BestEffortPrefetch();//true, 0);
            }

            else
            {
               printf("VALID ARGUEMENTS: NullPrefetch, AdjPrefetch, SeqPrefetch, BestEffortPrefetch\n");
               delete prefetch;
               exit(-1);
            }
   
          break;

        case '?':
          exit(-1);
          break;

        default:
          exit(-1);
        }
    }
  
   // tid_map is used to inform the simulator how
   // thread ids map to NUMA/cache domains. Using
   // the tid as an index gives the NUMA domain.
   unsigned int arr_map[] = {0};
   vector<unsigned int> tid_map(arr_map, arr_map +
         sizeof(arr_map) / sizeof(unsigned int));
   // The constructor parameters are:
   // the tid_map, the cache line size in bytes,
   // number of cache lines, the associativity,
   // the prefetcher object,
   // whether to count compulsory misses,
   // whether to do virtual to physical translation,
   // and number of caches/domains
   // WARNING: counting compulsory misses doubles execution time
   MultiCacheSystem sys(tid_map, 64, 1024, 64, prefetch, true, false, 1);
   char rw;
   uint64_t address;
   unsigned long long lines = 0;
   ifstream infile;
   // This code works with the output from the
   // ManualExamples/pinatrace pin tool
   infile.open("pinatrace.out", ifstream::in);
   assert(infile.is_open());

   string line;
   while(getline(infile,line))
   //for(int i = 0; i < 20; i++)
   {
      //getline(infile, line);
      stringstream ss(line);
      ss >> rw;
      assert(rw == 'R' || rw == 'W');
      ss >> hex >> address;
      if(address != 0) {
        //printf("INPUT: %s, ADDRESS: %" PRIu64 "\n\n", line.c_str(), address);
         // By default the pinatrace tool doesn't record the tid,
         // so we make up a tid to stress the MultiCache functionality
         sys.memAccess(address, rw, lines%2);
         //printf("-------------------------------------------------\n");
      }

      ++lines;
   }

   cout << "Accesses: " << lines << endl;
   cout << "Hits: " << sys.stats.hits << endl;
   cout << "Misses: " << lines - sys.stats.hits << endl;
   cout << "Local reads: " << sys.stats.local_reads << endl;
   cout << "Local writes: " << sys.stats.local_writes << endl;
   cout << "Remote reads: " << sys.stats.remote_reads << endl;
   cout << "Remote writes: " << sys.stats.remote_writes << endl;
   cout << "Other-cache reads: " << sys.stats.othercache_reads << endl;
   //cout << "Compulsory Misses: " << sys.stats.compulsory << endl;

   infile.close();
   delete prefetch;
   return 0;
}
